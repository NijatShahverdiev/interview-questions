## Map -> HashMap -> TreeMap

### HashMap
- HashMap can be tuned using the initialCapacity and loadFactor
- A HashMap requires way more memory than is needed to hold its data
- A HashMap shouldn't be more than 70% – 75% full. If it gets close, it gets resized and entries rehashed
- Rehashing requires n operations which is costly wherein our constant time insert becomes of 
order O(n)
- It's the hashing algorithm which determines the order of inserting the objects in the HashMap

The performance of a HashMap can be tuned by setting the custom initial capacity and the load factor
at the time of HashMap object creation itself.

##### However, we should choose a HashMap if:
- we know approximately how many items to maintain in our collection
- we don't want to extract items in a natural order

Under the above circumstances, HashMap is our best choice because it offers constant time insertion
search, and deletion.

### TreeMap

A TreeMap stores its data in a hierarchical tree with the ability to sort the elements with the help
of a custom Comparator.

A summary of its performance:
- TreeMap provides a performance of O(log(n)) for most operations like add(), remove() and contain()
- A Treemap can save memory (in comparison to HashMap) because it only uses the amount of memory needed to hold its items, unlike a HashMap which uses contiguous region of memory
- A tree should maintain its balance in order to keep its intended performance, this requires a
considerable amount of effort, hence complicates the implementation

##### We should go for a TreeMap whenever:

- memory limitations have to be taken into consideration
- we don't know how many items have to be stored in memory
- we want to extract objects in a natural order
- if items will be consistently added and removed
- we're willing to accept O(log n) search time


#### Q1: Why HashMap accepts at least one null key but TreeMap does not.
```
HashMap doesn't provide any guarantee over the way the elements are arranged in the Map.
It means, we can't assume any order while iterating over keys and values of a HashMap:
However, items in a TreeMap are sorted according to their natural order. If TreeMap objects cannot be
sorted according to natural order then we may make use of a 'Comparator' or 'Comparable' to define
the order, and comparator throws NullPointerException for null keys.
```