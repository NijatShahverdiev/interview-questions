1. * Equals Hashcode: https://www.baeldung.com/java-equals-hashcode-contracts
2. * Arraylist get() method: https://javatute.com/collection/how-get-method-of-arraylist-work-internally-in-java/ +
3. * Difference between ArrayList and LinkedList : https://www.javatpoint.com/difference-between-arraylist-and-linkedlist +
    In Doubly LinkedList each node has two links apart from storing data. First link points to  next node, second link points to previous node 
4. * Stream api , map, flatMap, method reference, join fork pool +
5.  Design Patterns, SOLID -
    1. Factory Pattern -  if we have multiple classes and we want to return any class behaviour during runtime process, instead of changing instatiating different class every time, we use factory pattery. It says that we have a superclass and its implementations and a factory class. Factory class defines which subclass will be returned during runtime using if-else or switch statements.
    2. Abstract Factory pattern - AFP is smilar to FP but is slightly different. We get rid of if-else statement using this pattern. AFP says that, we have Factory classes and their subclassed and an abstract factory class. This abstract factory class returns subclasses behavior based on input factory class.

    https://medium.com/@javatechie/solid-design-principle-java-ae96a48db97
    https://www.javatpoint.com/solid-principles-java
6.  Multithreading -
7. * Spring IOC, Dependecy Injection, contructor based injection ++
    https://reflectoring.io/constructor-injection/
8. * Transactional acid, proxy(how it works) +
9.  Object states(transient, managed, detached, removed) +
10. * Bean Scope +
11. Hibernate relationship best practices +
12. Microservice architecture, (scaling, how do microservices talk to each other, api gateway) - 
13. Apache Kafka + 
14. * Sql db index, isolation levels -
15. Your best solution in your projects
    
16. Unit test
    spy: spy are known as a partial mock object.It means spy creates a partial object or a half dummy of the real object by stubbing or spying the real ones. In spying, the real object remains unchanged, and we just spy some specific methods of it. In other words, we take the existing (real) object and replace or spy only some of its methods.Spies are useful when we have a huge class full of methods, and we want to mock certain methods. In this scenario, we should prefer using spies rather than mocks and stubs. It calls the real method behavior, if the methods are not stubbed.
    - https://javapointers.com/java/unit-test/difference-between-spy-and-mock-in-mockito/#:~:text=The%20difference%20is%20that%20in,not%20stub%20is%20do%20nothing.

    - parametrized unit testing is that , let's assume we have a test method and we want use it multiple time for different vaules.
        @ParameterizedTest
        @ValueSource(ints = {1, 3, 5, -3, 15, Integer.MAX_VALUE})
        public void test(int number){
            //do some test
        }

17. Law of Demeter
    - A method of an object can invoke another methods of that object
    - Method M can call objects as parameters M(Test test)
    - Method M can invoke objects created by that method
        class A{
            public void M(){
                Demo demo = new Demo();
            }
        }
    - Method M can invoke in object O can invoke any type of other objects that are direct componen of object O
        class A{
            //composition
            Demo demo;
            public void M(){
                demo = new Demo();
            }
        }
18. Bean lifecycle
    Generally lifecycle of an object is when&how it is born, how it behaves during its life, and when&how it dies. Properly in Spring  lifecycle of a bean starts with spring container starting. Firstly spring container gets started, then it creates an instance of a bean as per request, and then all necessary dependencies are injected. Finally, the bean is destroyed when the spring container is colsed. Therefor, sometimes we need to make custom initialization and destroying. We can implement init() and destroy() method and @PostConstructor and @PreDestroy annotations properly in Spring Boot.
19. * How does HashMap work internally
    HashMap uses its static inner class Node<K, V> for storing its entries. This means that each entry is a Node in the HashMap. Internally HashMap uses hashcode of key object and this hashcode is used to find a bucket index where new entry can be added.
    When we add new entry to the HashMap, hashcode is used to check if there is already a key with the same hashcode or not.
    If there already a key with the same hashcode, then equals() method is used for these keys. If equals() method returns true, it means that there is already a node with the same hashcode and its hence value. So old node is replaced with the new one, otherwise if the equals() method returns false, then that entry is stored in a new node inside singly linked list but in the same bucker.
    If there is no a key with the given hashcode, then the new node is stored in different bucket.

20. * @Profile() - profiles in spring defines that we can use our profiles for different environments such as dev, prod and etc.
    Profiles can be activated in different ways. We can activate profile in class level, xml conficuration, or in .properties files
    I prefer configure profiles inside .properties files. For instance I want to use multiple datasource in my application for different environmens. In a single properties file I can configure multiple datasource, and I just need to write 
    
    my.prop=used-always-in-all-profiles
    #---
    spring.config.activate.on-profile=dev
    spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
    spring.datasource.url=jdbc:mysql://localhost:3306/db
    spring.datasource.username=root
    spring.datasource.password=root
    #---
    spring.config.activate.on-profile=production
    spring.datasource.driver-class-name=org.h2.Driver
    spring.datasource.url=jdbc:h2:mem:db;DB_CLOSE_DELAY=-1
    spring.datasource.username=sa
    spring.datasource.password=sa

21. open-in-view:
    When using open session in view anti-pattern, our service layer opens and close hibernate session. 
    After service layer committed sql transation, there is no an active transaction. But due to open-in-view preperty, still there will be active session to perform lazily fetched association operation. This increases load in the database server, and after each transaction the database has to write transaction logs to the disk which is an expensive operation. Moreover, there can be a risk our lazily fetched association return different data than our service layer used to perform the business logic, because we have 2 or more
    independent transaction. Due to that we have to close open-in-view option in properties or yaml conf.

22. Composition advantage over Inheritance
23. ClassNotFoudException vs NoClassDefFoundError 
24. Hibernate findAll antipattern for spesific field
    https://vladmihalcea.com/spring-data-findall-anti-pattern/
25. Spring AOP
26. ApplicationContext https://www.baeldung.com/spring-application-context
27. LazyInitializationException - https://thorben-janssen.com/lazyinitializationexception/#:~:text=Hibernate%20throws%20the%20LazyInitializationException%20when,client%20application%20or%20web%20layer.

28. DTO projection - https://thorben-janssen.com/entities-dtos-use-projection/
29. Types of interface - https://www.geeksforgeeks.org/types-of-interfaces-in-java/
30. * If two servie classes call each other, there will be cycling. How to handle this situation.
31. * Hibernate N+1 problem, and its solution.
